#!/bin/bash
p=${HOME}/Downloads/server/config
echo "application properties for $1 "
git_path=${p}/$1
echo "$git_path"

git add $git_path

git commit -m "Application Properties Push"

echo "GIT COMMITED"
echo "————————————————————"

echo "Calling URI (POST): " $2

curl -X POST $2/actuator/refresh
